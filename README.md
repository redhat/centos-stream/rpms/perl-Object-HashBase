# perl-Object-HashBase

This package is used to generate classes based on hash references. Using this
class will give you a new() method, as well as generating accessors you
request.  Generated accessors will be getters, set_ACCESSOR setters will also
be generated for you. You also get constants for each accessor (all caps)
which return the key into the hash for that accessor. Single inheritance is
also supported.